import kubernetes
from kubernetes import client, config
import json

config.load_config()
print('Loaded incluster_config')

v1 = client.CoreV1Api()
print('Using Client')

api_instance = client.CustomObjectsApi()

group = 'kubeflow.org'
version = 'v1'
plural = 'profiles'
#body = {"spec":{"resourceQuotaSpec":{"hard":{"pods":"11"}}}}
body = {"spec":{"resourceQuotaSpec":None}}

profiles = api_instance.list_cluster_custom_object(group=group, version=version, plural=plural)
for profile in profiles['items']:
    name = profile['metadata']['name']
    print(name)
    api_response = api_instance.patch_cluster_custom_object(group=group, version=version, plural=plural, name=name, body=body)
    print(api_response)
