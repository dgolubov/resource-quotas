# About

Scripts to manage resource quotas in the profiles of a Kubeflow cluster.
Mainly - script to patch (delete) quotas in all profiles.

This is to be used when we change default quotas.
Default quotas will be set by profile controller, if no quotas are found in the profile specs.

## How to run?

Run from lxplus as a Python script